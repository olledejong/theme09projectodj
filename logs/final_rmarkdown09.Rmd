---
title: "Machine learning model for detecting cervical cancer"
author: "Olle de Jong"
date: "September 11, 2018"
output: 
  pdf_document:
    fig_caption: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(cache = TRUE)
knitr::opts_chunk$set(echo = FALSE)
```

```{r imports}
library(ggplot2)
library(gmodels)
library(pander)
#install.packages("RWeka")
library(RWeka)
```

## Introduction ##

This project focuses on cervical cancer [1]. The cervix is the lowers part of the uterus and connects the uterus with the vagina.
This type of cancer occurs when the cells of the cervix grow much faster than normally. Although cervical cancer is a "slow"
growing cancer, it can spread from the cervix to other organs and parts of the body, like the lungs, liver, bladder, vagina, and rectum.
Because cervical cancer is slow growing, precancerous changes can be detected and prevented early on. Because detecting a cancer early on
is so important, it would be very beneficial to be able to detect cervical cancer by using machine learning.

In this research screening techniques have been used for three of the attributes. All of these techniques are branched off of the general
diagnostic procedure colposcopy [2] (by Hans Hinselmann). Colposcopy is a procedure to examine an illuminated, magnified view of the cervixand tissues of the vigina and vulva. Colposcopy provides an enlarged view of the areas, allowing the colposcopist to visually distinguish normal from abnormal appearing tissue.  

So, for this project I would like to research if it is in any way possible to predict if a client is 
suffering cervical cancer by using the medical based data and a machine learning model.  
Hypothesis: Yes, because of the 858 instances I think there is enough data available.

### The Data ###

Data [3] used for this research project was originally obtained by the 'Hospital Universitario de Caracas' in Caracas, Venezuela [4].
There are 36 attributes and the class attribute for this research is: Biopsy. Biopsy has two possible values, 1 and 0 (boolean).
Where 1 represents a positive biopsy on cervical cancer. Therefore 0 represents a negative biopsy on cervical cancer. All other 
attributes are either integers/floats (which can vary greatly) or booleans.  

Some of the attributes were obtained by a survey kind of approach, others using screening strategies like colposcopy using 
acetic acid - Hinselmann, colposcopy using Lugol iodine - Schiller[5], cytology[6] and biopsy.

Data obtained by 'Hospital Universitario de Caracas' in Caracas, Venezuela.  
https://archive.ics.uci.edu/ml/datasets/Cervical+cancer+%28Risk+Factors%29  
Direct link for instant download:  
https://archive.ics.uci.edu/ml/machine-learning-databases/00383/risk_factors_cervical_cancer.csv  
Hospital Universitario de Carcas, Venezuela  
http://huc.org.ve/  

\newpage

## Material & Methods ##


## Exploratory Data Analysis ##

### Loading Data ###

```{r load_data}
#get current working directory
getwd()
#set working directory to where the risk_factors_cervical_cancer.csv file is 
#located (for people who reproduce this, it will probably be another directory)
#setwd("../data")
#import data and convert the "?" (missing) values to NA strings.
orig_cc_data <- read.csv("risk_factors_cervical_cancer.csv", header = T, na.strings = "?")
cc_data <- read.csv("risk_factors_cervical_cancer.csv", header = T, na.strings = "?")
```
Basic information about the data:  
Instances: 858  
Attributes: 36  
Missing values: Yes

The features in this dataset cover demographic information, habits, and historic medical records.
As described in the introduction of this research, most values in this dataset are booleans, the others are integers.

Missing values in this dataset are especially interesting since there must be a reason for a person to skip a question
or to leave one blank. Therefor you can kind of predict what that missing value indicates, though you can never be sure.
Also, you can not just delete every instance that contains a missing value, because of the attributes; STDs: Time since 
first diagnosis and STDs: Time since last diagnosis. For these two attributes its normal that there's a missing value, 
because all who have never been diagnosed with a STD will leave the question blank.  

In this dataset, you can not identify any specific outliers. Most of this data has been obtained by a survey so some of 
the values may seem like an outlier, but in fact these are just legit and realistic answers.  

### Exploratory Data Analysis ###

```{r rough_analysis}
#to get rid of the weird values, round everything to one decimal
#cc_data <- round(cc_data, digits = 1)

#compactly display structure
str(cc_data)

#sum of NA per column
na_count <-sapply(cc_data, function(y) sum(is.na(y)))
pander(na_count)
```
Above you see a small dataframe of the NA count per attribute (column). In columns "STDs..Time.since.first.diagnosis" and "STDs..Time.since.last.diagnosis" the missing values are there because the question is inapplicable to that specific person. 
All other missing values are caused because an individual did not want to answer that specific question, and therefor left it blank.  

The class attribute is unevenly represented, as you can see above.
In total there are 858 instances (rows).
The class attribute "Biopsy" is a boolean. Two options, positive or negative.
The number of instances positive on biopsy: 55
The number of instances negative on biopsy: 803

```{r ggplots}
ggplot(data = cc_data, aes(x = Age)) +
  geom_bar() +
  labs(title = "Distribution of attribute; Age",
       x = "Age",
       y = "Number of instances (individuals)")

ggplot(data = cc_data, aes(x = First.sexual.intercourse)) +
  theme_bw() +
  geom_bar() +
  labs(title = "Distribution of age of first sexual intercourse",
       x = "Age",
       y = "Number of instances (individuals)")
```
Figures 1 and 2. The distribution of the attributes; Age and First Sexual Intercourse.  
NOTE: The 7 removed rows are caused by NA values.

\pagebreak
```{r crosstables}
#below there will be some code that executes the crosstable function.
#there might be information to get out of those results.
#STDs / Biopsy
with(cc_data, CrossTable(STDs, Biopsy))
#Schiller / Biopsy
with(cc_data, CrossTable(Schiller, Biopsy))
#Cotology / Biopsy
with(cc_data, CrossTable(Citology, Biopsy))
#Hinselmann / Biopsy
with(cc_data, CrossTable(Hinselmann, Biopsy))
#Previous diagnosed with cervical cancer / Biopsy
with(cc_data, CrossTable(Dx.Cancer, Biopsy))
```
Figures 3, 4, 5, 6 and 7. All are crosstables.  
All values are booleans, where 1 indicates yes and 0 indicates a no.  

From crosstable 1 you can learn:  
Total individuals with a STD and not suffering from cervical cancer: 67/700 which is 10.8 %  
Total individuals with a STD and suffering from cervical cancer: 12/53 which is 22.6 %  
This might indicate that having an STD has a connection with suffering from cervical cancer.  

From crosstable 2 you can learn:  
First off, Schiller [5] is a diagnostic technique which is based on staining. This technique is performed
by applying an Iodine solution to the cevix under direct vision. Normal tissue stains brown, whereas abnormal 
tissue like cervical cancer, does not stain.  
You immediately notice that 48 out of 74 (which is 64.9%) who test positive on Schiller's test, also test
positive on biopsy for cervical cancer. This might indicate that Schiller's test is quite decisive when it
comes to determining if a person is suffering from cervical cancer.  

From crosstable 3 you can learn:  
18 out of 44 individuals who test positive on citology, also test positive on biopsy for cervical cancer. Again,
This might be an interesting attribute when trying to predict if someone is suffering from cervical cancer.  

From crosstable 4 you can learn:  
25 out of 33 individuals who test positive on Hinselmann's test, also test positive on biopsy for cervical cancer.
This could also be a very decisive attribute that can be of great value further on in this project.

From crosstable 5 you can learn:  
6 out of 18 individuals who where previously diagnosed with cervical cancer, are suffering from cervical cancer
again, according to the biopsy class attribute.  

For the following crosstable I've changed the values from numerical to categorical so it can be easier displayed and
understood. You will learn more about this further on in this project.  

```{r crosstable_sex_intercourse}
#change values of first sexual intercourse to categorical values
#create "normal" class for attribute
fsi_early = c(10:15)
cc_data$First.sexual.intercourse[cc_data$First.sexual.intercourse %in% fsi_early] = "Early"
#create "normal" class for attribute
fsi_normal = c(16:20)
cc_data$First.sexual.intercourse[cc_data$First.sexual.intercourse %in% fsi_normal] = "Normal"
#create "late" class for attribute
fsi_late = c(21:45)
cc_data$First.sexual.intercourse[cc_data$First.sexual.intercourse %in% fsi_late] = "Late"
#create labels for new attribute(column)
cc_data$First.sexual.intercourse = factor(cc_data$First.sexual.intercourse, 
                                          levels=c("Early", "Normal", "Late"))
#create cross table of the categorical version of the first sexual intercourse attribute and the biopsy attribute
with(cc_data, CrossTable(First.sexual.intercourse, Biopsy))
```
Figure 8: crosstable of the attributes first sexual intercourse (categorical) and Biopsy.  

From this crosstable you can learn:
Total individuals with categorical class "Early" and suffering cervical cancer: 14/277 which is 5.1%
Total individuals with categorical class "Normal" and suffering from cervical cancer: 39/506 which is 7.7%
Total individuals with categorical class "Late" and suffering from cervical cancer: 2/66 which is 3.0%
This information suggests that there might be a connection between the attribute; age of first sexual intercourse 
and suffering from cervical cancer. A thing you might see is that it seems that the chance of getting cervical
cancer is greater when the age of first sexual intercourse is "normal" or "early" rather than "late".  

## Creating Clean Data / Modifying Data for ML algorithms ##

When exploring the data manually, there was something I immediately noticed. Two instances have a greater value for
attribute; STD: Time since first diagnose, than their own age. In the case of these two individuals the STD in
question is HIV, HIV is know to be able to transfer onto an unborn baby, so for now I decide not to delete these
two instances. If this will cause any trouble further on in this project, I will definitely delete them.  

There is no data that has been deleted. Below I've made some modifications to the data so the ML algorithms might work better.
Because of changing some columns from numerical to categorical the data becomes easier to read and also some algorithms handle
it better.

For creating Figure 8 I had to create a categorical variant of the First.sexual.intercourse column. I categorized this
column to my own perception and I'm going to do this for the for the following attributes as well:  
- Age  
- Number of sexual partners  
- Number of pregnancies
- Smokes (years)  
- Smokes (packs a year)  
- Hormonal contraceptives (years)  
- IUD (years)  

Categorizing these attribute is because the machine learning algorithms might work better with "bagged" attributes.

For Age  
```{r categorize_age}
#change values of age to categorical values in same column
#create "normal" class for attribute
young_ages = c(0:26)
cc_data$Age[cc_data$Age %in% young_ages] = "Young"
#create "midlife" class for attribute
midlife_ages = c(26:49)
cc_data$Age[cc_data$Age %in% midlife_ages] = "Midlife"
#create "late" class for attribute
older_ages = c(50:100)
cc_data$Age[cc_data$Age %in% older_ages] = "Older"
#create labels for new attribute (column)
cc_data$Age = factor(cc_data$Age, levels=c("Young", "Midlife", "Older"))
```

Now for Number of sexual partners  
```{r categorize_number_of_sexual_partners}
#change values of attribute: Number of sexual partners to categorical values in same column
#create "few" class for attribute
few = c(1:2)
cc_data$Number.of.sexual.partners[cc_data$Number.of.sexual.partners %in% few] = "Few"
#create "normal" class for attribute
normal = c(2:5)
cc_data$Number.of.sexual.partners[cc_data$Number.of.sexual.partners %in% normal] = "Normal"
#create "many" class for attribute
many = c(6:99)
cc_data$Number.of.sexual.partners[cc_data$Number.of.sexual.partners %in% many] = "Many"
#create labels for new attribute (column)
cc_data$Number.of.sexual.partners = factor(cc_data$Number.of.sexual.partners, 
                                           levels=c("Few", "Normal", "Many"))
```

Now for Number of pregnancies  
```{r categorize_number_of_pregnancies}
#change values of attribute: Number of pregnancies to categorical values in same column
#create "none" class for attribute
preg_none = c(0)
cc_data$Num.of.pregnancies[cc_data$Num.of.pregnancies %in% preg_none] = "None"
#create "normal" class for attribute
preg_normal = c(1,2,3)
cc_data$Num.of.pregnancies[cc_data$Num.of.pregnancies %in% preg_normal] = "Normal"
#create "many" class for attribute
preg_many = c(4:20)
cc_data$Num.of.pregnancies[cc_data$Num.of.pregnancies %in% preg_many] = "Many"
#create labels for new attribute (column)
cc_data$Num.of.pregnancies = factor(cc_data$Num.of.pregnancies, 
                                    levels=c("None", "Normal", "Many"))
```

Now for Smokes (years)  
```{r categorize_smokes_years}
#change values of attribute: Smokes (years) to categorical values in same column
#create "None" class for attribute
smokes_none_years = c(0)
cc_data$Smokes..years.[cc_data$Smokes..years. %in% smokes_none_years] = "None"
#create "Few" class for attribute
smokes_few_years = seq(1, 4, 0.1)
cc_data$Smokes..years.[cc_data$Smokes..years. %in% smokes_few_years] = "Few"
#create "Normal" class for attribute
smokes_normal_years = seq(5, 15, 0.1)
cc_data$Smokes..years.[cc_data$Smokes..years. %in% smokes_normal_years] = "Normal"
#create "Many" class for attribute
smokes_many_years = seq(16, 99, 0.1)
cc_data$Smokes..years.[cc_data$Smokes..years. %in% smokes_many_years] = "Many"
#create labels for new attribute (column)
cc_data$Smokes..years. = factor(cc_data$Smokes..years., 
                                levels=c("None", "Few", "Normal", "Many"))
```

Now for Smokes (packs a year)
```{r categorize_smokes_packsayear}
#change values of attribute: Smokes (packs a year) to categorical values in same column
#create "none" class for attribute
packs_none = c(0)
cc_data$Smokes..packs.year.[cc_data$Smokes..packs.year. %in% packs_none] = "None"
#create "some" class for attribute
packs_some = seq(1, 5, 0.1)
cc_data$Smokes..packs.year.[cc_data$Smokes..packs.year. %in% packs_some] = "Some"
#create "normal" class for attribute
packs_normal = seq(5.1, 15, 0.1)
cc_data$Smokes..packs.year.[cc_data$Smokes..packs.year. %in% packs_normal] = "Normal"
#create "many" class for attribute
packs_many = seq(15.1, 60, 0.1)
cc_data$Smokes..packs.year.[cc_data$Smokes..packs.year. %in% packs_many] = "Many"
#create labels for new attribute (column)
cc_data$Smokes..packs.year. = factor(cc_data$Smokes..packs.year.,
                                     levels=c("None", "Some", "Normal", "Many"))
```

Now for Hormonal contraceptives (years)  
```{r categorize_hormonal_contraceptives}
#change values of attribute: Hormonal contraceptives (years) to categorical values in same column
#create "none" class for attribute
contraceptives_none = c(0)
cc_data$Hormonal.Contraceptives..years.[cc_data$Hormonal.Contraceptives..years. 
                                        %in% contraceptives_none] = "None"
#create "some" class for attribute
contraceptives_some = seq(1, 3, 0.1)
cc_data$Hormonal.Contraceptives..years.[cc_data$Hormonal.Contraceptives..years. 
                                        %in% contraceptives_some] = "Some"
#create "normal" class for attribute
contraceptives_normal = seq(3.1, 16, 0.1)
cc_data$Hormonal.Contraceptives..years.[cc_data$Hormonal.Contraceptives..years. 
                                        %in% contraceptives_normal] = "Normal"
#create "many" class for attribute
contraceptives_many = seq(16.1, 60, 0.1)
cc_data$Hormonal.Contraceptives..years.[cc_data$Hormonal.Contraceptives..years. 
                                        %in% contraceptives_many] = "Many"
#create labels for new attribute (column)
cc_data$Hormonal.Contraceptives..years. = factor(cc_data$Hormonal.Contraceptives..years.,
                                                 levels=c("None", "Some", "Normal", "Many"))
```

Now for IUD (Years)
```{r categorize_iud_years}
#change values of attribute: IUD (years) to categorical values in same column
#create "none" class for attribute
iud_none = c(0)
cc_data$IUD..years.[cc_data$IUD..years. %in% iud_none] = "None"
#create "some" class for attribute
iud_some = seq(1, 3, 0.1)
cc_data$IUD..years.[cc_data$IUD..years. %in% iud_some] = "Some"
#create "normal" class for attribute
iud_normal = seq(3.1, 9, 0.1)
cc_data$IUD..years.[cc_data$IUD..years. %in% iud_normal] = "Normal"
#create "many" class for attribute
iud_many = seq(9.1, 60, 0.1)
cc_data$IUD..years.[cc_data$IUD..years. %in% iud_many] = "Many"
#create labels for new attribute (column)
cc_data$IUD..years. = factor(cc_data$IUD..years., 
                             levels=c("None", "Some", "Normal", "Many"))
```

\pagebreak

### Determine quality metrics relevant for your research ###

![](../data/qualitymatrix.png)
Figure 9: Quality matrix.  

Quality matrix: False omission rate (FOR) = sum ( False negative ) / sum ( Predicted condition negative )  
The reason I have chosen this quality matrix is because this topic, cervical cancer, is a very serious one. Because we
do not want people who are actually ill sent home with the message: "You are healthy", it is of great importance
that the number of false negatives are as low as possible. With screening people on a specific desease comes great
risk and responsibility, and therefor keeping the false negatives as low as possible is really important. Though, 
keeping the false positives as low as possible is also important. This could prevent a client from actually believing
he or she is ill, and, it will possibly save money and resources because of not needing to screen the client any further.  

## Results & Discussion ##

To investigate the performance of machine learning (ML) algorithms I am going to use WEKA. Waikato Environment for Knowledge Analysis (WEKA) is a suite of machine learning software written in Java, developed at the University of Waikato, New Zealand [6].  

To start working on this, the modified data of this project so far has to be saved.  

```{r save_modified_data}
getwd()
setwd("../data")
#transform numeric to nominal values for usage in weka
#columns that need to be transformed
cols <- c(8, 10, 12, 14:25, 29:36)
#use a lapply to use factor funtion over all columns in cols vector
cc_data[cols] <- lapply(cc_data[cols], factor)
#write modified file using arff extension
write.arff(cc_data, file = "mod_risk_factors_cervical.arff")
```

The modified data has been imported to WEKA and using WEKA all standard ML-algorithms have been investigated. These standard ML-algorithms are
following:  
- ZeroR  
- OneR  
- Naive Bayes  
- SimpleLogistic  
- SMO  
- IBk  
- J48  
- RandomForest  

After loading in the results of an experimentor session with the eigth machine learning algorithms that were chosen to be tested, their will follow visualizations to see the performance of the eight different machine learning algorithms which have been used. These figures are made using the performance file of the experimentor session. You can find this file in the data folder, named "results_new.csv"

```{r ml_results}
getwd()
setwd("../data")
ml_results <- read.csv("results_new.csv", sep = ",")
```

```{r ml_results_statistics_percorrect}
ggplot(data = ml_results, aes(y = Percent_correct, x = Key_Scheme)) +
  geom_boxplot() +
  labs(title = "Boxplot of percentage correctly classified per algorithm",
       x = "Algorithm",
       y = "Percentage correct") +
  scale_x_discrete(labels = c("Naive Bayes", "SMO", "Simple Logistics",
                              "IBK", "OneR", "ZeroR", "J48", "Random Forest"))
```
Figure 10: Boxplot of percentage correctly classified per algorithm. No further explanation needed.  

\pagebreak
```{r ml_results_statistics_falsnegrate}
ggplot(data = ml_results, aes(y = False_negative_rate, x = Key_Scheme)) +
  geom_boxplot() +
  labs(title = "Boxplot of the false negative rate per algorithm",
       x = "Algorithm",
       y = "False negative rate") +
  scale_x_discrete(labels = c("Naive Bayes", "SMO", "Simple Logistics", 
                              "IBK", "OneR", "ZeroR", "J48", "Random Forest"))
```
Figure 11: Boxplot of the false negative rate per algorithm. Including ZeroR.  

This boxplot tells something about the rate of instances classified as false negatives. As described at the begin of this project
the goal is to keep the number of false negatives as low as possible. By looking at this boxplot figure you can see well every
machine learning algorithm is performing at once, which is usefull for determining which line of research will probably be most 
effective when investigating further.  

\pagebreak
```{r ml_results_statistics_kappa}
ggplot(data = ml_results, aes(y = Kappa_statistic, x = Key_Scheme)) +
  geom_boxplot() +
  labs(title = "Boxplot of the kappa statistic per algorithm",
       x = "Algorithm",
       y = "Kappa statistic value") +
  scale_x_discrete(labels = c("Naive Bayes", "SMO", "Simple Logistics", 
                              "IBK", "OneR", "ZeroR", "J48", "Random Forest"))
```
Figure 12: Boxplot of kappa statistic per algorithm.  

Evaluating the boxplots which were made above, you can tell that all algorithms are quite close in performance except for Naive Bayes
and Random Forest. Naive bayes truely has some terrible scores in all three boxplots. Not only are the results fluctuating a lot, the
average of the scores is also really not good enough. Random Forest has quite a good false negative rate, but when looking at the
percentage correct it isnt that awesome and when looking at the kappa statistic, it becomes certain I do not want to investigate this 
algoritm any further. By looking at these boxplots there are really only three algorithms who are worth looking into further. These are
SMO, Simple Logistics and J48.

So, to find out which of these three algorithms is the best for this perticular dataset, some further tests have been run for two of the
three algorithms which's settings are modifiable (SMO & J48). Simple Logistics doesnt have options you're able to modify which it may 
increase the algorithm's performance.  

```{r smo_tests}
#load test results (outcome of weka experimenter)
getwd()
setwd("../data")
smotests <- read.csv("testsSMO.csv", sep = ",")

#create boxplot for percentage correct of the SMO algorithm with different settings
ggplot(data = smotests, aes(y = Percent_correct, x = Key_Scheme_options)) +
  geom_boxplot() +
  labs(title = "Boxplot of percentage correctly classified per setting (SMO)",
       x = "Algorithm",
       y = "Percentage correct") +
  scale_x_discrete(labels = c("SMO c = 1.0", "SMO c = 1.5", "SMO c = 0.5"))

#create boxplot for false negative rate of the SMO algorithm with different settings
ggplot(data = smotests, aes(y = False_negative_rate, x = Key_Scheme_options)) +
  geom_boxplot() +
  labs(title = "Boxplot of false negative rate per setting (SMO)",
       x = "Algorithm",
       y = "False negative rate") +
  scale_x_discrete(labels = c("SMO c = 1.0", "SMO c = 1.5", "SMO c = 0.5"))
```
Figures 13 and 14. Boxplot of the false negative rate and percentage correct for the SMO algorithm with two different settings.  

The setting c = 0.5 seems to do best when aiming for a good percentage of correctly classified instances and a false negative
rate which is as low as possible.  

\pagebreak

```{r j48_tests}
#load test results (outcome of weka experimenter)
getwd()
setwd("../data")
J48tests <- read.csv("testsJ48.csv", sep = ",")

#create boxplot for percentage correct of the J48 algorithm with different settings
ggplot(data = J48tests, aes(y = Percent_correct, x = Key_Scheme_options)) +
  geom_boxplot() +
  labs(title = "Boxplot of percentage correctly classified per setting (J48)",
       x = "Algorithm",
       y = "Percentage correct") +
  scale_x_discrete(labels = c("J48 c = 0.25", "J48 c = 0.15", "J48 splits = T and c = 0.25"))

#create boxplot for percentage correct of the J48 algorithm with different settings
ggplot(data = J48tests, aes(y = False_negative_rate, x = Key_Scheme_options)) +
  geom_boxplot() +
  labs(title = "Boxplot of false negative rate per setting (J48)",
       x = "Algorithm",
       y = "False negative rate") +
  scale_x_discrete(labels = c("J48 c = 0.25", "J48 c = 0.15", "J48 splits = T and c = 0.25"))
```
Figures 15 and 16. Boxplots of different settings of the J48 algorithm.  

The standard settings (c = 0.25) seem the best settings.  

So, we can now conclude there are two algorithms which are performing best of all. These are; SMO with setting: c = 0.5 and J48 with
setting: c = 0.25. With these two models I can work on the assignments to come.

I am not so sure if these findings are the best findings possible, since this is the first time I've done something like this. Though,
these results are possibly correct. But on the other hand, there are some things there could be done to look at these algorithms
differently, and maybe you would get totally different results after doing that. One of these things is performing attribute selection,
I have not done this and this might change the outcome of the performance of these algorithms. I must say that the data isnt that ideal
as well. Compared to other people their datasets, this one is one of the smallest and because only 55 out of the 858 individuals are
actually sick there isnt really that much data the algorithms can use to learn. I would've liked to have a much greater dataset but
unfortunately you can't just add some data to this dataframe. So, the hypothesis I stated at the begin of this paper has not been proved right, now we can say that the goal has not been acchieved.

### Project Proposal ###

Since this research using this particular dataset has not brought results that are really worth looking into further, it would be pointless to propose any followup project. This is because, described earlier in this paper, the dataset just doesn't suffice. However, if a more balanced and bigger dataset would be provided it would be really interesting to make a web based tool which could in any way predict if a woman should be checked more thorough in a hospital. Of course, since you can not say that the results of a machine learning model are one hundred percent correct a web based tool result would certainly not be binding. It could only tell someone that they might have a higher risk of getting cervical cancer, and encourage them to be examined by a professional. With a project like this, extra importance goes out to disclaimers etc. since you, as an individual or company, do not want to be sued because someone used the tool and got the result; "You're not likely to be at any more risk than the average woman" and still suffered cervical cancer.

\pagebreak

[1] Cervical Cancer.  
    https://www.webmd.com/cancer/cervical-cancer/cervical-cancer#1  
[2] Colposcopy by Hans Hinselmann  
    https://en.wikipedia.org/wiki/Colposcopy  
[3] Data obtained by 'Hospital Universitario de Caracas' in Caracas, Venezuela.  
    https://archive.ics.uci.edu/ml/datasets/Cervical+cancer+%28Risk+Factors%29  
    Direct link for instant download:  
    https://archive.ics.uci.edu/ml/machine-learning-databases/00383/risk_factors_cervical_cancer.csv  
[4] Hospital Universitario de Carcas, Venezuela  
    http://huc.org.ve/  
[5] Schiller  

[6] WEKA
    https://en.wikipedia.org/wiki/Weka_(machine_learning)  

Copyright (c) 2018 Olle de Jong.  
Licensed under GPLv3. See gpl.md  


